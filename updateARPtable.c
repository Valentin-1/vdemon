#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <net/ethernet.h>

#include "logging.h"
#include "libnetlink.h"

#define ATTR_BUF_SIZE 1010


static int updateARP_n(const struct sockaddr_nl *who, struct nlmsghdr *nlmp, void *arg)
{
	if(NULL == nlmp || NULL == arg)
	{
		m_infoMsg_thr("Error in updateARP: no data\n");
		return -1;
	}

	int newIfid = ((int*)arg)[1];
	struct ndmsg* rtmp = NLMSG_DATA(nlmp);

	/// пропустить сообщение, если интерфейс локальный или ARP запись удалена. ///
	if( 1 == rtmp->ndm_ifindex || NUD_FAILED == rtmp->ndm_state)
		return 0;

///--- init nlmsg ---///
	struct {
		struct nlmsghdr	nh;
		struct ndmsg nd;
		char attrbuf[ATTR_BUF_SIZE];
	} req = {
		.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg)),
		.nh.nlmsg_type = RTM_NEWNEIGH,

		.nh.nlmsg_seq = nlmp->nlmsg_seq++,
		.nh.nlmsg_pid = getpid(),

		.nd.ndm_family = rtmp->ndm_family,
	};

	struct rtattr *rtatp = (struct rtattr*)(((char*)(rtmp)) + NLMSG_ALIGN(sizeof(struct ndmsg)));
	int rtAttrlen = NLMSG_PAYLOAD(nlmp, sizeof(struct ndmsg));
	int rtDatlen, errStatus;
	__u8 *rtdatp;
	for ( ; RTA_OK(rtatp, rtAttrlen); rtatp = RTA_NEXT(rtatp, rtAttrlen) )
	{
		rtdatp = RTA_DATA(rtatp);
		rtDatlen = RTA_PAYLOAD(rtatp);
		switch (rtatp->rta_type)
		{
		case NDA_DST :
			if( 0 > (errStatus = addattr_l(&req.nh, sizeof(req), NDA_DST, rtdatp, rtDatlen)) )
				return errStatus;
			break;
		case NDA_LLADDR :
			if( 0 > (errStatus = addattr_l(&req.nh, sizeof(req), NDA_LLADDR, rtdatp, rtDatlen)) )
				return errStatus;
			break;
		}
	}

	struct rtnl_handle rth = { .fd = ((int*)arg)[0]};

///--- ARP flush all ---///
	req.nd.ndm_ifindex = rtmp->ndm_ifindex;
	req.nd.ndm_state = NUD_FAILED;
	if( 0 > rtnl_send_check(&rth, &req, req.nh.nlmsg_len) )
	{
		m_infoMsg_thr("Error rtnl_send when ARP flush\n");
		return -2;
	}


///--- ARP add/chg ---///
	req.nd.ndm_ifindex = newIfid;
	req.nd.ndm_state = NUD_REACHABLE;
	if( 0 > rtnl_send_check(&rth, &req, req.nh.nlmsg_len) )
	{
		m_infoMsg_thr("Error rtnl_send when ARP add/chg\n");
		return -2;
	}

	return 0;
}



int updateARP(int ifid)
{
	if(1 > ifid)
	{
		m_infoMsg_thr("Error: updateARP invalid interface id = %d\n", ifid);
		return -1;
	}

	struct {
		struct nlmsghdr nlh;
		struct ndmsg nd;
	} req = {0};
	struct rtnl_handle rth = {0};

	if( 0 > rtnl_open(&rth, 0) )
		return -1;

	if( 0 > rtnl_dump_request(&rth, RTM_GETNEIGH, &req, sizeof(req)) )
	{
		m_perrMsg("rtnl_dump_request");
		rtnl_close(&rth);
		return -1;
	}


	int args[2];
	args[0] = rth.fd;
	args[1] = ifid;
	if( 0 > rtnl_dump_filter_nc(&rth, updateARP_n, args, 0) )
	{
		m_perrMsg_thr("rtnl_dump_filter");
		rtnl_close(&rth);
		return -1;
	}

	rtnl_close(&rth);

	return 0;
}