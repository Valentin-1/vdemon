#ifndef CONFIG
#define CONFIG 1

#include "cJSON.h"
#include "logging.h"

#include <stdio.h> //FILE
#include <stdint.h> //#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h> //inet_aton
#include <net/if.h>

#define IPV4_STR_SIZE 16

uint8_t testInterval; //Health monitor interval in seconds
uint8_t failRetries; //Attempts before WAN failover
uint8_t recoveryRetries; //Attempts before WAN recovery
uint16_t icmpTimeout; //Health monitor ICMP timeout in seconds

typedef struct provider_s provider_t;
struct provider_s
{
	char *name;  //provider name
	int8_t priority;
	char interface[IFNAMSIZ]; //interface str. IFNAMSIZ is defined in net/if.h and linux/if.h
	unsigned int ifid; //interface id
	struct in_addr gate;
	uint8_t ipv4Count;
	struct in_addr *ipv4; //Набор целевых IP адресов
	provider_t *next;
};

typedef struct threadParams_s threadParams_t;
struct threadParams_s {
	provider_t* ISP;
	uint priorityRoute; //limit is UINT32_MAX
	uint8_t exitFlag;
};

int getConfig(char* fileName, provider_t** ISP);
void destructConfig(provider_t* ISP);
int showConfig(provider_t* ISP, FILE *stream);


#endif //end_CONFIG
