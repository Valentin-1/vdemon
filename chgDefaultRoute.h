#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h> //inet_pton()
#include <linux/rtnetlink.h>

#include "logging.h"
#include "libnetlink.h"

/*The changeMainRoute function adds or changes default route*/
int changeMainRoute(struct in_addr *ipv4gate, int idx);

/*The addDefRoutePriority function changes default route with priority.
* Supported two nlmsg types:
* RTM_DELROUTE for removal,
* RTM_NEWROUTE for add and don't replace if the route already exists.
*/
int chgDefRoutePriority(struct in_addr *ipv4gate, int idx, int priority, int nlmsg_type);
