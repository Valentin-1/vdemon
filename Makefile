ifeq ($(name),)
	name := "MultiWANd.x"
endif

FILES = cJSON.c config.c main.c vDemon.c logging.h icmp.c libnetlink.c chgDefaultRoute.c updateARPtable.c
CC = gcc
STD = -std=gnu99
KEYS = -Wall

all:
	$(CC) -o $(name) $(STD) $(FILES) -lpthread
	
dbg1:
	$(CC) -o $(name) $(STD) $(FILES) -lpthread -DDEBUG

dbg2:
	$(CC) -o $(name) $(STD) $(FILES) -lpthread -DDEBUG=2

dbg3:
	$(CC) -o $(name) $(STD) $(FILES) -lpthread -DDEBUG=3

dbgcfg:
	$(CC) -o $(name) $(STD) $(FILES) -lpthread -DDBGCFG

