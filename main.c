#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
//#include <stdlib.h>
//#include <arpa/inet.h>
#include "config.h"
#include "logging.h"

#define FNAME_SIZE_MAX 255
#define LOG_PREFIX "log_"

extern int vDemon(provider_t *ISPs);
char* LOG_FILE_NAME;

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		printf("few args, expected file name in *.json format\n");
		//!add struct config file: helpFunction
		return -1;
	}

	int errStatus;
	pid_t pid;
	provider_t *ISPs = NULL;

	/*get log file name*/
	char logFname[FNAME_SIZE_MAX] = LOG_PREFIX;
	if(NULL == strrchr(argv[0], '/'))
		strcat(logFname, argv[0]);
	else
		strcat(logFname, strrchr(argv[0], '/') + 1);
	LOG_FILE_NAME = logFname;

	/*get demon configuration*/
	if( getConfig(argv[1], &ISPs) )
	{
		printf("ERROR: can't getConfig\n");
		return -1;
	}

	pid = fork();
	if(pid < 0)
	{
		perror("Start vDemon failed");
		return -1;
	}
	else if(0 == pid)
	{
		umask(0);
		if (0 > setsid() )
			perror("setsid");
		if (0 > chdir("/var/") )
			perror("chdir");

		printf("Demon started\n");
//		if(0 != close(STDIN_FILENO) )
//			m_perrMsg("close(STDIN_FILENO)");
		if(0 != close(STDOUT_FILENO) )
			m_perrMsg("close(STDOUT_FILENO)");
		if(0 != close(STDERR_FILENO) )
			m_perrMsg("close(STDERR_FILENO)");

		errStatus = vDemon(ISPs);
		m_infoMsg("- Demon Stopped with errStatus: %d\n", errStatus);
		destructConfig(ISPs);
		return errStatus;
	}
	else if(pid)
	{
		return 0;
	}
	else
		fprintf(stderr, "Error in main: impossible outcome\n" );
	return -1;
}