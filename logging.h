#ifndef LOGGING
#define LOGGING 1

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

enum {
	ERR_LOG = -9, //ошибка при выводе сообщения в лог
	ERR_PRM, //ошибка в данных переданных функции
	ERR_WRG = -7, //(wrong)
	ERR_IMP, //(impassible) невозможное значение
	ERR_FF, //(finaly faild)
};

extern char* LOG_FILE_NAME;

#define m_infoMsg(msg, ...) do{ \
	FILE* fp; \
	if( NULL == (fp = fopen(LOG_FILE_NAME, "a")) ) \
		return ERR_LOG; \
	if(0 > fprintf(fp, msg, ##__VA_ARGS__)) \
	{} \
	if(fclose(fp)) \
		return ERR_LOG; \
}while(0)

#define m_dbgMsg(log, ...) m_infoMsg("%s:%d: " log, __FILE__, __LINE__, ##__VA_ARGS__)
#define m_perrMsg(log, ...) m_infoMsg(log ": %m\n", ##__VA_ARGS__)


static pthread_mutex_t mxLog;

#define m_infoMsg_thr(msg, ...) do{ \
	FILE* fp; \
	if( pthread_mutex_lock(&mxLog) ) \
	{} \
	if( NULL == (fp = fopen(LOG_FILE_NAME, "a")) ) \
		pthread_exit(0); \
	if(0 > fprintf(fp, msg, ##__VA_ARGS__)) \
	{} \
	if(fclose(fp)) \
		pthread_exit(0); \
	if( pthread_mutex_unlock(&mxLog) ) \
	{} \
}while(0)

#define m_dbgMsg_thr(log, ...) m_infoMsg_thr("%s:%d: " log, __FILE__, __LINE__, ##__VA_ARGS__)
#define m_perrMsg_thr(log, ...) m_infoMsg_thr(log ": %m\n", ##__VA_ARGS__)


#if(DEBUG)
#define m_dbg1(log, ...) m_infoMsg_thr("[DEBUG 1] " log, ##__VA_ARGS__)
#else
#define m_dbg1
#endif

#if(DEBUG > 1)
#define m_dbg2(log, ...) m_infoMsg_thr("[DEBUG 2] " log, ##__VA_ARGS__)
#else
#define m_dbg2
#endif

#if(DEBUG > 2)
#define m_dbg3(log, ...) m_infoMsg_thr("[DEBUG 3] " log, ##__VA_ARGS__)
#else
#define m_dbg3
#endif

#if(DBGCFG)
#define m_dbgcfg(log, ...) printf("[DBG CFG] :%d: " log, __LINE__, ##__VA_ARGS__)
#else
#define m_dbgcfg
#endif

#endif //end LOGGING
