#include "config.h"
#include "optList.h"

#define m_jsonAssertNull(pointer, objName) \
		if(NULL == pointer) { \
			printf("Config file does not exist object \"%s\"\n", objName); \
			return -2; \
		}
#define m_assertNull(pointer, msg) \
		if(NULL == pointer) { \
			perror(msg); \
			return -errno; \
		}
#define BUFF_SIZE 1020


static int readConfigFile(char *configFileName, char *request_body)
{
	if(NULL == configFileName)
	{
		printf("---%s invalid input data\n", __FUNCTION__);
		return ERR_PRM;
	}

	int errStatus = 0;
	FILE * fp = fopen(configFileName, "r");
	m_assertNull(fp, "fopen");

	char buff[BUFF_SIZE] = {0};
	long cursorPos;
	long countRead = 0, i = 0;
	while(!feof(fp))
	{
		if(NULL != fgets(buff, BUFF_SIZE, fp))
		{
			if( 0 > (cursorPos = ftell(fp)) )
			{
				perror("ftell");
			}
			countRead = cursorPos - i;
			m_dbgcfg("buff: %s countRead=%ld\n", buff, countRead); //dbg
			if(i > BUFF_SIZE - countRead)
			{
				printf("low BUFF_SIZE\n");
				errStatus -2;
				break;
			}
			memcpy(&request_body[i], buff, countRead);
			i += countRead;
		}
	}

	if(fclose(fp))
	{
		printf("Error fclose %s ", configFileName );
		perror("");
		return -errno;
	}

	return errStatus;
}

static int check_uint8_t(char *buff)
{
	if(NULL == buff)
		return ERR_PRM;
	if(buff[0] < '0' || buff[0] > '9')
		return -2;
	int a = atoi(buff);
	if(a > UINT8_MAX || a < 0)
		return -3;
	else
		return 0;
}
static int check_uint16_t(char *buff)
{
	if(NULL == buff)
		return ERR_PRM;
	if(buff[0] < '0' || buff[0] > '9')
		return -2;
	int a = atoi(buff);
	if(a > UINT16_MAX || a < 0)
		return -3;
	else
		return 0;
}

static int setOptions(cJSON* item, provider_t **prov, int startOpt)
{
	m_dbgcfg("---setOptions---\n");
	if(NULL == item)
		return ERR_PRM;
	provider_t *ISP = *prov;

	char buff[BUFF_SIZE] = {0};
	option_t option[] = optList_enum;
	cJSON *obj;
	int arrSize;
	char *nameT;
	char strIPv4[IPV4_STR_SIZE];
	int i, z, n;
	int errStatus = 0;

	for(i = startOpt; i < OPT_MAX; i++)
	{
		obj = cJSON_GetObjectItem(item, option[i].name);
		m_jsonAssertNull(obj, option[i].name);

		nameT = cJSON_PrintUnformatted(obj);
		if(NULL != nameT)
		{
			memcpy(buff, "\0", 1);
			memcpy(buff, nameT+1, strlen(nameT)-2 ); //+1 и -2 избавляет от кавычек
		}
		else
		{
			printf("Error %s\n", option[i].name);
			return -1;
		}
		m_dbgcfg("nameT: %s\n", nameT);

		switch(option[i].type)
		{
		case UNVERIFABLE:
			break;
		case UINT8_T:
			if( 0 > (errStatus = check_uint8_t(buff)) )
			{
				printf("%s Error: argument \"%s\" is wrong: %s\n", ISP->name, option[i].name, buff);
				return errStatus;
			}
			break;
		case UINT16_T:
			if( 0 > (errStatus = check_uint16_t(buff)) )
			{
				printf("%s Error: argument \"%s\" is wrong: %s\n", ISP->name, option[i].name, buff);
				return errStatus;
			}
			break;
		default:
			printf("%s Error: impossible type: %d\n", ISP->name, option[i].type);
			return ERR_IMP;

		}

		switch(option[i].id)
		{
		case HEALTH_INTERVAL:
			testInterval = atoi(buff);
			m_dbgcfg("case HEALTH_INTERVAL: %d \n", testInterval);
			break;
		case HEALTH_FAIL_RETRIES:
			failRetries = atoi(buff);
			m_dbgcfg("case HEALTH_FAIL_RETRIES: %d\n", failRetries);
			break;
		case HEALTH_RECOVERY_RETRIES:
			recoveryRetries = atoi(buff);
			m_dbgcfg("case HEALTH_RECOVERY_RETRIES: %d\n", recoveryRetries);
			break;
		case ICMP_RECV_TIMEOUT:
			icmpTimeout = atoi(buff);
			m_dbgcfg("case ICMP_RECV_TIMEOUT: %d\n", icmpTimeout);
			break;
		case PROVIDERS_LIST:
			if( 0 == (arrSize = cJSON_GetArraySize(obj)) )
			{
				printf("providersList is empty\n");
				return -2;
			}
			obj = obj->child;
			for(z = 0; z < arrSize; z++)
			{
				provider_t *newISP = (provider_t*) malloc(sizeof(provider_t));
				m_assertNull(newISP, "malloc(provider_t)");
				memset(newISP, '\0', sizeof(provider_t));

				if( 0 > (n = setOptions(obj, &newISP, PROVIDERS_LIST+1)) )
				{
					printf("case PROVIDERS_LIST: error setOptions\n");
					return -2;
				}
				if(NULL != obj->next)
					obj = obj->next;

				/// ORDER BY priority DESC ///
				if(ISP)
					if(newISP->priority < ISP->priority)
					{
						provider_t* ISPtmp = ISP;
						while(ISPtmp->next)
						{
							if(newISP->priority < ISP->next->priority)
								ISPtmp = ISPtmp->next;
							else
								break;
						}
						newISP->next = ISPtmp->next;
						ISPtmp->next = newISP;
					}
					else
					{
						newISP->next = ISP;
						ISP = newISP;
					}
				else
					ISP = newISP;
			}
			i += n;
			break;
		case NAME:
			ISP->name = (char*)malloc(strlen(nameT));
			m_assertNull(ISP->name, "malloc(provider name)");
			memcpy(ISP->name, nameT, strlen(nameT) );
			m_dbgcfg("case NAME: %s\n", ISP->name);
			break;
		case PRIORTY:
			ISP->priority = atoi(buff);
			m_dbgcfg("%s: case PRIORTY: %d\n", ISP->name, ISP->priority);
			break;
		case INTERFACE:
			memcpy(ISP->interface, buff, strlen(nameT)-2 );
			if( !(ISP->ifid = if_nametoindex(ISP->interface)) )
			{
				printf("Error: argument \"%s\" is wrong: %s\n", option[i].name, ISP->interface);
				perror("if_nametoindex");
				return -errno;
			}
			m_dbgcfg("case INTERFACE: %s, id = %d\n", ISP->interface, ISP->ifid);
			break;
		case GATE:
			memcpy(strIPv4, nameT+1, strlen(nameT)-2 );
			memset(strIPv4 + strlen(nameT)-2, '\0', 1 );
			m_dbgcfg("case GATE: %s\n", strIPv4);//dbg
			if ( 1 != inet_pton(AF_INET, strIPv4, &(ISP->gate)) )
			{
				printf("%s Error: argument \"%s\" is wrong: %s\n", ISP->name, option[i].name, buff);
				printf("Error setOptions: inet_pton()\n");
				return -3;
			}
			break;
		case IP:
			if( 0 == (ISP->ipv4Count = cJSON_GetArraySize(obj)) )
			{
				printf("ip addresses is missing for %s\n", ISP->name);
				return -1;
			}

			ISP->ipv4 = (struct in_addr*) malloc(sizeof(struct in_addr) * ISP->ipv4Count);
			m_assertNull(ISP->name, "malloc(ipv4)");

			for (z = 0; z < ISP->ipv4Count; z++)
			{
				nameT = cJSON_Print(cJSON_GetArrayItem(obj, z));
				if(nameT)
				{
					memcpy(strIPv4, nameT+1, strlen(nameT)-2 );
					memset(strIPv4 + strlen(nameT)-2, '\0', 1 );
					m_dbgcfg("case IP: strIPv4: %s\n", strIPv4);//dbg
					if ( 1 != inet_pton(AF_INET, strIPv4, &(ISP->ipv4[z])) )
					{
						printf("%s Error: argument \"%s\" is wrong: %s\n", ISP->name, option[i].name, buff);
						printf("Error setOptions: inet_pton()\n");
						return -3;
					}
				} else {
					printf("Error cJSON_Print. Can't create %s\n", buff);
					return -3;
				}
			}
			break;

		default:
			printf("unsupported id option: %d\n", option[i].id);
		}

	}
	*prov = ISP;
	m_dbgcfg("---end setOptions---\n");
	return OPT_MAX;
}

int getConfig(char *configFileName, provider_t **prov)
{
	if(NULL == configFileName)
		return ERR_PRM;

	int errStatus;

	/// file content to char array as request_body ///
	char request_body[BUFF_SIZE];
	if(0 > (errStatus = readConfigFile(configFileName, request_body)) )
	{
		printf("Error readConfigFile\n");
		return errStatus;
	}

	m_dbgcfg("%s: request_body:\n%s\n", __FUNCTION__, request_body); //dbg

	/// parse request_body ///
	cJSON* request_json = NULL;

	request_json = cJSON_Parse(request_body);
	if (NULL == request_json)
	{
		printf("Error JSON parse\n");
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr)
		{
			printf("before token: %s\n", error_ptr);
		}
		cJSON_Delete(request_json);
		return -2;
	}

	/// create providers list with properties as struct provider_t ///
	if(0 > setOptions(request_json, prov, 0))
	{
		printf("Error setOptions\n");
		cJSON_Delete(request_json);
		return -2;
	}

	cJSON_Delete(request_json);
	m_dbgcfg("configuration completed successfully\n");
	return 0;
}

int showConfig(provider_t *data, FILE *stream)
{
	extern char *LOG_FILE_NAME;
	FILE *fp = stream;
	if(NULL == stream)
		if( NULL == (fp = fopen(LOG_FILE_NAME, "a")) )
			return -2;

	if(0 > fprintf(fp, "testInterval: %d\n", testInterval))
		m_perrMsg("showConfig");
	if(0 > fprintf(fp, "failRetries: %d\n", failRetries))
		m_perrMsg("showConfig");
	if(0 > fprintf(fp, "recoveryRetries: %d\n", recoveryRetries))
		m_perrMsg("showConfig");
	if(0 > fprintf(fp, "icmpTimeout: %d\n", icmpTimeout))
		m_perrMsg("showConfig");

	int curIPv4 = 0;
	for(; data; data = data->next)
	{
		if(0 > fprintf(fp, "Provider name: %s\n", data->name))
			m_perrMsg("showConfig");
		if(0 > fprintf(fp, " priority: %d\n", data->priority))
			m_perrMsg("showConfig");
		if(0 > fprintf(fp, " interface: %s\n", data->interface))
			m_perrMsg("showConfig");
		if(0 > fprintf(fp, " ifid: %d\n", data->ifid))
			m_perrMsg("showConfig");
		if(0 > fprintf(fp, " gate: %s\n", inet_ntoa(data->gate)))
			m_perrMsg("showConfig");
		if(0 > fprintf(fp, "ip:\n"))
			m_perrMsg("showConfig");
		for(curIPv4 = 0; curIPv4 < data->ipv4Count; curIPv4++)
			if(0 > fprintf(fp, " %s\n", inet_ntoa(data->ipv4[curIPv4])))
				m_perrMsg("showConfig");
		if(0 > fprintf(fp, "\n"))
			m_perrMsg("showConfig");
	}

	if(NULL == stream)
		if(fclose(fp))
			return -3;
	return 0;
}

/// free resources ///
void destructConfig(provider_t *ISP)
{
	provider_t *tmpISP;
	while(ISP)
	{
		tmpISP = ISP;
		ISP = ISP->next;
		free(tmpISP->ipv4);
		free(tmpISP->name);
		free(tmpISP);
	}
}