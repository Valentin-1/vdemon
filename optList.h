#ifndef OPT_LIST
#define OPT_LIST

/// enum for value ///
enum {
	HEALTH_INTERVAL,
	HEALTH_FAIL_RETRIES,
	HEALTH_RECOVERY_RETRIES,
	ICMP_RECV_TIMEOUT,
	PROVIDERS_LIST,
	NAME,
	PRIORTY,
	INTERFACE,
	GATE,
	IP,
	OPT_MAX,
};

enum {
	UNVERIFABLE,
	UINT8_T,
	UINT16_T,
	TYPE_MAX
};


typedef struct {
	uint8_t id;
	uint8_t type;
	const char *name;
} option_t; //for optList_enum

#define optList_enum {\
	{HEALTH_INTERVAL, UINT8_T, "health_interval"},\
	{HEALTH_FAIL_RETRIES, UINT8_T, "health_fail_retries"},\
	{HEALTH_RECOVERY_RETRIES, UINT8_T, "health_recovery_retries"},\
	{ICMP_RECV_TIMEOUT, UINT16_T, "icmp_recv_timeout"},\
	{PROVIDERS_LIST, UNVERIFABLE, "providersList"},\
	{NAME, UNVERIFABLE, "name"},\
	{PRIORTY, UINT8_T, "priority"},\
	{INTERFACE, UNVERIFABLE, "interface"},\
	{GATE, UNVERIFABLE, "gate"},\
	{IP, UNVERIFABLE, "ip"},\
	}

#endif//end OPT_LIST
