
#include <pthread.h>

#include "vDemon.h"

extern void* icmpWork(void *params);

static uint8_t *exitA; // глобально для handlerSig()
static provider_t *hsISPs; // для handlerSig()

static void handlerSig(int signo);

#define OPENING_PRIORITY 2400

int vDemon(provider_t *ISPs)
{
	if(NULL == ISPs)
	{
		m_infoMsg("Error input data: NULL == ISPs\n");
		return ERR_PRM;
	}
	hsISPs = ISPs;
	if (SIG_ERR == signal(SIGTERM, handlerSig)) //15
		m_infoMsg("can't catch SIGTERM");
	if (SIG_ERR == signal(SIGUSR1, handlerSig)) //10
		m_infoMsg("can't catch SIGUSR1");
	if (SIG_ERR == signal(SIGUSR2, handlerSig)) //12
		m_infoMsg("can't catch SIGUSR2");

	void *errP = NULL;
	m_infoMsg("- Demon Started -\n");

	threadParams_t params = {
		.ISP = ISPs,
		.priorityRoute = OPENING_PRIORITY,
		.exitFlag = 0
	};
	exitA = &params.exitFlag;

	pthread_t tid;
	if(0 != pthread_create(&tid, NULL, icmpWork, &params) )
	{
		m_infoMsg("Error pthread_create in %s\n", __FUNCTION__);
		return -2;
	}
	if(0 != pthread_join(tid, &errP) )
	{
		m_infoMsg("Error pthread_join in %s\n", __FUNCTION__);
		return -2;
	}

	m_dbg3("sucsess end %s file\n", __FUNCTION__);//dbg
	if(NULL != errP)
		return *(int*)errP;
	return 0;
}



static void handlerSig(int signo)
{
	FILE* fp;
	switch (signo)
	{
	case SIGTERM: //terminate
		*exitA = 1;
		if( NULL == (fp = fopen(LOG_FILE_NAME, "a")) )
			return;
		if(0 > fprintf(fp, "SIGTERM: Demon is stopping\n"))
		{}//some error
		if(fclose(fp))
			return;
		break;
	case SIGUSR2: //show config
		m_infoMsg_thr("SIGUSR2: vDemon confguration:\n-----\n");
		if(0 > showConfig(hsISPs, NULL))
		{}//some error
		break;
	}
	return;
}