#include "chgDefaultRoute.h"

static int errStatus;
static
struct {
	struct nlmsghdr	nh;
	struct rtmsg r;
	char attrbuf[4096];
} req = {

	.r.rtm_family = AF_INET,
	.r.rtm_table = RT_TABLE_MAIN,
	.r.rtm_protocol = RTPROT_BOOT,
	.r.rtm_scope = RT_SCOPE_UNIVERSE,
	.r.rtm_type = RTN_UNICAST
};


static int
changeDefaultRoute(struct in_addr *ipv4gate, int idx)
{
	errStatus = 0;
	struct rtnl_handle rth;
	if ((errStatus = rtnl_open(&rth, RTMGRP_IPV4_IFADDR)) < 0)
	{
		m_perrMsg_thr("rtnl_open");
		return errStatus;
	}

	req.nh.nlmsg_seq = rth.seq;
	req.nh.nlmsg_pid = getpid();

	if( 0 > (errStatus = addattr32(&req.nh, sizeof(req), RTA_TABLE, RT_TABLE_MAIN)) )
		goto quit;
	if( 0 > (errStatus = addattr_l(&req.nh, sizeof(req), RTA_GATEWAY, ipv4gate, 4)) ) //!hard
		goto quit;
	if( 0 > (errStatus = addattr32(&req.nh, sizeof(req), RTA_OIF, idx)) )
		goto quit;

	if(0 > (errStatus = rtnl_send_check(&rth, &req, req.nh.nlmsg_len)) )
	{
		m_perrMsg_thr("rtnl_send %d", errStatus);
		goto quit;
	}

quit:
	rtnl_close(&rth);
	return errStatus;
}

int changeMainRoute(struct in_addr *ipv4gate, int idx)
{
	if(NULL == ipv4gate || 1 > idx)
		return ERR_PRM;

	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));
	req.nh.nlmsg_flags = NLM_F_REQUEST | NLM_F_REPLACE | NLM_F_CREATE;
	req.nh.nlmsg_type = RTM_NEWROUTE;


	if(0 > (errStatus = changeDefaultRoute(ipv4gate, idx)) )
	{
		m_infoMsg_thr("Error changeMainRoute\n");
		return errStatus;
	}

	return 0;
}


int chgDefRoutePriority(struct in_addr *ipv4gate, int idx, int priority, int nlmsg_type)
{
	if(NULL == ipv4gate || 1 > idx || 1 > priority || UINT32_MAX < priority)
		return ERR_PRM;

	if(RTM_NEWROUTE == nlmsg_type)
	{
		req.nh.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL;
		req.nh.nlmsg_type = RTM_NEWROUTE;
	}
	else if (RTM_DELROUTE == nlmsg_type)
	{
		req.nh.nlmsg_flags = NLM_F_REQUEST;
		req.nh.nlmsg_type = RTM_DELROUTE;
	}
	else
	{
		m_infoMsg_thr("Error chgDefRoutePriority: unsupported nlmsg type\n");
		return -2;
	}

	req.nh.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg));

	if( 0 > (errStatus = addattr32(&req.nh, sizeof(req), RTA_PRIORITY, priority)) )
		return errStatus;

	if(0 > (errStatus = changeDefaultRoute(ipv4gate, idx)) )
	{
		m_infoMsg_thr("Error chgDefRoutePriority\n");
		return errStatus;
	}

	return 0;
}
