#include <stdio.h>		//dbg
#include <stdlib.h>		//exit(), EXIT_FAILURE
#include <errno.h>		// errno, perror()
#include <unistd.h>      // close(), sleep()
#include <string.h>           // strcpy, memset(), memcpy()
#include <fcntl.h>

#include <sys/socket.h> //socket
#include <sys/types.h> //u_int8_t 
#include <sys/ioctl.h> //ioctl()

#include <netinet/in.h>       // IPPROTO_ICMP, INET_ADDRSTRLEN
#include <netinet/ip.h>       // struct iphdr and IP_MAXPACKET (which is 65535)
#include <netinet/ip_icmp.h>  // struct icmphdr, ICMP_ECHO
#include <net/ethernet.h>		//struct ether_header, ETHER_ADDR_LEN, ETHERTYPE_IP, ETHER_TYPE_LEN, ETHER_HDR_LEN //ETH_P_IP = 0x0800, ETH_P_IPV6 = 0x86DD
#include <net/if.h>           // struct ifreq
#include <linux/if_packet.h>  // struct sockaddr_ll
#include <arpa/inet.h> //inet_aton()
#include <netdb.h>

#include <pthread.h>

#include "config.h"
#include "logging.h"
#include "chgDefaultRoute.h"

#define SIZE_ICMP_DATA 1024
#define MAX_FAILS 3


struct icmpPckt_s {
	struct icmphdr icmpHead;
	char icmpMsg[SIZE_ICMP_DATA];
};
typedef struct completionArgs_s {
	int *socksd;
	int *sockrd;
	struct in_addr *ipv4gate;
	int *idx;
	int *priority;
	pthread_t *tidnextISP;
	uint8_t *NextISPexitF;
	char *ISPname;
} completionArgs_t;

extern int updateARP(int ifid);
static unsigned short checksumICMP(void *b, int len);
static int completion(completionArgs_t *args, void *errP);

/// set ICMP header ///
/*
struct icmphdr
{
  u_int8_t type;		// message type /
  u_int8_t code;		// type sub-code /
  u_int16_t checksum;
  union
  {
	struct
	{
	  u_int16_t	id;
	  u_int16_t	sequence;
	} echo;			// echo datagram /
	u_int32_t	gateway;	// gateway address /
	struct
	{
	  u_int16_t	__glibc_reserved;
	  u_int16_t	mtu;
	} frag;			// path mtu discovery /
  } un;
};
*/



void* icmpWork(void *params)
{
	int errStatus = 0;
	void *const errP = (void*)&errStatus;
	void *errNumP = NULL;

	if(NULL == params)
	{
		m_infoMsg_thr("Error input data: NULL == params\n");
		errStatus = ERR_PRM;
		pthread_exit(errP);
	}

	provider_t* ISP = ((threadParams_t*)params)->ISP;
	uint8_t *myExitFlag = &(((threadParams_t*)params)->exitFlag);
	int myPriorityRt = ((threadParams_t*)params)->priorityRoute;
	m_infoMsg_thr("Start ISP %s\n", ISP->name);

	threadParams_t nextISP = {
		.ISP = ISP->next,
		.exitFlag = 0,
	};
	pthread_t tidnextISP = 0;

	struct icmpPckt_s icmpPcktSend = {{0}};

	memcpy(icmpPcktSend.icmpMsg, ISP->name, strlen(ISP->name));
	icmpPcktSend.icmpHead.type = ICMP_ECHO;
	icmpPcktSend.icmpHead.un.echo.sequence = htons(1); //!hard for test

	int curIPv4 = 0;
	struct sockaddr_in sa;
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = ISP->ipv4[curIPv4].s_addr; //ISP->chpt->ipv4.s_addr;

	icmpPcktSend.icmpHead.checksum = checksumICMP( &icmpPcktSend, sizeof(icmpPcktSend) );

	size_t frame_length = sizeof(icmpPcktSend.icmpHead) + strlen(ISP->name);

	int socksd, sockrd;
	completionArgs_t cmlArgs = {
		.socksd = &socksd,
		.sockrd = &sockrd,
		.ipv4gate = &(ISP->gate),
		.idx = &(ISP->ifid),
		.priority = &myPriorityRt,
		.tidnextISP = &tidnextISP,
		.NextISPexitF = &(nextISP.exitFlag),
		.ISPname = ISP->name,
	};
///---send options---///
	socksd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (socksd  < 0 )
	{
		m_perrMsg_thr("Error socket");
		errStatus = -errno;
		completion(&cmlArgs, errP);
	}
	if( 0 > fcntl (socksd, F_SETFL, O_NONBLOCK) )
	{
		m_perrMsg_thr("Error fcntl");
		errStatus = -errno;
		completion(&cmlArgs, errP);
	}

	if(0 > setsockopt(socksd, SOL_SOCKET, SO_BINDTODEVICE, ISP->interface, strlen(ISP->interface) ) )
	{
		m_perrMsg_thr("Error setsockopt");
		errStatus = -errno;
		completion(&cmlArgs, errP);
	}

///---recv options---///
	sockrd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_IP)); //SO_RCVTIMEO
	if(sockrd < 0)
	{
		m_perrMsg_thr("Error sockrd");
		errStatus = -errno;
		completion(&cmlArgs, errP);
	}

	struct sockaddr_ll sll = {
		.sll_family = AF_PACKET,
		.sll_ifindex = ISP->ifid,
	};
	if (bind(sockrd, (struct sockaddr *)&sll, sizeof(sll)) < 0)
	{
		m_perrMsg_thr("Error bind sockrd socket");
		errStatus = -errno;
		completion(&cmlArgs, errP);
	}

	char buff[SIZE_ICMP_DATA] = {0};
	struct iphdr *ipHead = (struct iphdr*) &buff[ sizeof(struct ether_header)];
	struct icmpPckt_s *icmpRecv = (struct icmpPckt_s*) &buff[ sizeof(struct iphdr) + sizeof(struct ether_header)]; //сдвиг на размер ip + ethernet заголовка
///---end recv options---///

	u_short err, attempt, renewal;
	short hostsCount = (short)ISP->ipv4Count; //кол-во проверяемых хостов.
	short IwasAvailable = 1; //флаг доступности меня (текущего потока), как провайдера
	int nready; // для select()

	attempt = failRetries;
	renewal = recoveryRetries;
	struct timeval tv = {
		.tv_sec = icmpTimeout,
		.tv_usec = 0
	};

	if(0 > changeMainRoute(&(ISP->gate), ISP->ifid))
	{
		m_infoMsg_thr("Error: change Default Route\n");
	}
	do {
		errStatus = chgDefRoutePriority(&(ISP->gate), ISP->ifid, myPriorityRt, RTM_NEWROUTE);
		if(-EEXIST == errStatus)//на случай, если такой маршрут создан другим процессом
		{
			myPriorityRt++;
			m_infoMsg_thr("Used another priority value\n");
			continue;
		}
		else if(0 > errStatus)
		{
			m_infoMsg_thr("Error: add Route with priority\n");
			errStatus = -errno;
			completion(&cmlArgs, errP);
		}
	} while(errStatus);
	if(0 > updateARP(ISP->ifid))
	{
		m_infoMsg_thr("Error: updateARP\n");
	}

	for(err = 0; err < MAX_FAILS; )
	{
		m_dbg2("%s: myExitFlag = %d\n", ISP->name, *myExitFlag);
		if(*myExitFlag) // надо ли мне (потоку) завершиться?
		{	//перед самозавершением, заврешаем созданный поток.
			errStatus = 0;
			completion(&cmlArgs, errP);
		}

		m_dbg2("%s: sending\n", ISP->name);//dbg
		if (( sendto(socksd, &icmpPcktSend, frame_length, 0, (struct sockaddr *)&sa, sizeof(sa)) ) <= 0)
		{
			m_perrMsg_thr("Error sendto()");
			err++;
			continue;
		}
#if defined(DEBUG)
		char strIP[INET_ADDRSTRLEN];//dbg
		if(NULL == inet_ntop(AF_INET, (const void*)&sa.sin_addr, strIP, INET_ADDRSTRLEN) )//dbg
		{
			m_perrMsg_thr("[DEBUG 1] Error NtoP");
		}
		m_infoMsg_thr("[DEBUG 1] %s::sent to %s\n", ISP->name, strIP);//dbg
#endif

		err = 0;
		tv.tv_sec = icmpTimeout;
		tv.tv_usec = 0;
		while(err < MAX_FAILS)
		{
			fd_set fds;
			FD_ZERO(&fds);
			if (sockrd != 0)
				FD_SET(sockrd, &fds);
			FD_SET(0, &fds);

			// wait for data
			nready = select(sockrd + 1, &fds, (fd_set *) 0, (fd_set *) 0, &tv);
			if (nready < 0) {
				m_perrMsg_thr("Error select:");
				errStatus = -errno;
				completion(&cmlArgs, errP);
			}
			else if (nready == 0) { //время вышло или принамал не свои пакеты, например unreachable
				attempt--;
				renewal = recoveryRetries;
				break;
			}
			else if (sockrd != 0 && FD_ISSET(sockrd, &fds))
			{

				m_dbg2("%s: receiving\n", ISP->name);//dbg
				if(recvfrom(sockrd, &buff, SIZE_ICMP_DATA, 0, NULL, NULL ) <= 0) //!?EAGAIN
				{
					m_perrMsg_thr("Error: recvfrom()");
					err++;
					continue;
				}

				/*ожидать прихода следующего пакета если не icmp или поле дата не соответствует ожидаемому*/
				if((IPPROTO_ICMP != ipHead->protocol) || memcmp(icmpRecv->icmpMsg, ISP->name, strlen(ISP->name)))
					continue;

				m_dbg2("%s::icmpType: icmp.type %d ?= ICMP_ECHOREPLY %d\n", ISP->name, icmpRecv->icmpHead.type, ICMP_ECHOREPLY); //dbg
				m_dbg2("%s::icmpData: %s\n", ISP->name, icmpRecv->icmpMsg ); //dbg
				m_dbg2("%s::checksum: %d == %d // пока не проверяется\n", ISP->name, icmpRecv->icmpHead.checksum, checksumICMP( &icmpRecv, sizeof(icmpRecv)) ); //dbg
				/*проверка типа icmp сообщения (должно быть ICMP_ECHOREPLY) и контрольной суммы icmp заголовка*/
				if( (icmpRecv->icmpHead.type == ICMP_ECHOREPLY)/* &&
				 ( icmpRecv->icmpHead.checksum == checksumICMP( &icmpRecv, sizeof(icmpRecv)) ) */
				  )
				{	//хост доступен снова или остается доступным
					if(IwasAvailable) 	//был ли доступен, как провайдер?
					{
						break;
					}
					else if(renewal) //если нет - дожидемся стабильности
					{
						renewal--;
						break;
					}
					else	//затем посылается сигнал завершения созданного потока
						//изменяются таблицы маршрутизации и ARP
					{
						m_infoMsg_thr("%s: available again\n", ISP->name);//dbg

						IwasAvailable = 1;
						hostsCount = (short)ISP->ipv4Count;
						nextISP.exitFlag = 1; //условие завершения созданног потока
						if(tidnextISP)
						{
							if(0 != (errStatus = pthread_join(tidnextISP, &errNumP)) )
							{
								m_infoMsg_thr("Error pthread_join: %s\n", strerror(errStatus));
							}
							if(NULL != errNumP)
								if(0 != *(int*)errNumP)
									m_infoMsg_thr("%s: The thread %s completion with status %d\n", ISP->name, nextISP.ISP->name, *(int*)errNumP);
							tidnextISP = 0;
						}

						if(0 > changeMainRoute(&(ISP->gate), ISP->ifid))
						{
							m_infoMsg_thr("Error: change Default Route\n");
						}
						if(0 > updateARP(ISP->ifid))
						{
							m_infoMsg_thr("Error: updateARP\n");
						}
						break;
					}
				}
				else //хост не доступен. Уменьшается кол-во попыток для его проверки.
				{
					m_dbg1("%s: host unreachable\n", ISP->name);//dbg
					attempt--; //кол-во попыток проверки одного хоста
					renewal = recoveryRetries;
					break;
				}

				m_dbg1("%s::icmpRecv->icmpMsg %s\n", ISP->name, icmpRecv->icmpMsg);//dbg

			}
		}

		m_dbg3("%s::attempts= %d; hostsCount = %d\n", ISP->name, attempt, hostsCount);
		if(testInterval + tv.tv_sec > icmpTimeout) //обеспечим равномерный интервал между тестами (с точностью до 1 сек)
			sleep(testInterval - icmpTimeout + tv.tv_sec);

		if(!attempt) //исчерпалось кол-во проверок хоста.
			//далее меняется проверяемый хост.
			//И если это был последний проверяемый хост, то создается новый поток и переключается интерфейс (провайдер)
		{
			renewal = recoveryRetries;
			attempt = failRetries;
			curIPv4++;
			if(curIPv4 == ISP->ipv4Count)// curIPv4 = curIPv4->next; //меняется проверяемый хост
				curIPv4 = 0;
			else if(curIPv4 < 0 || curIPv4 >= ISP->ipv4Count)
			{
				m_infoMsg_thr("wrong curIPv4 value = %d\n", curIPv4);
				errStatus = ERR_WRG;
				completion(&cmlArgs, errP);
			}
			m_dbg1("%s: next host\n", ISP->name); //dbg
			sa.sin_addr.s_addr = ISP->ipv4[curIPv4].s_addr;//curIPv4->ipv4.s_addr;
			if(hostsCount > 0 && hostsCount <= ISP->ipv4Count)
			{
				hostsCount--;
			}
			if(hostsCount == 0) 	//более не осталось доступных хостов,
				//создается новый поток
			{
				hostsCount--; //исключим спам одинаковых потоков
				if(ISP->next)
				{
					m_infoMsg_thr("%s: unavailable\n", ISP->name);
					IwasAvailable = 0;
					nextISP.priorityRoute = 1 + myPriorityRt;
					nextISP.exitFlag = 0;
					if(0 != (errStatus = pthread_create(&tidnextISP, NULL, icmpWork, &nextISP)))
					{
						m_infoMsg_thr("Error pthread_create: %s\n", strerror(errStatus));
					}
				} else
				{
					m_infoMsg_thr("no working ISPs\n");
				}
			} else if(hostsCount == -1)
			{
				continue;
			} else if(!(hostsCount > 0 && hostsCount <= ISP->ipv4Count))
			{
				m_infoMsg_thr("wrong hostsCount value = %d\n", hostsCount);
				errStatus = ERR_WRG;
				completion(&cmlArgs, errP);
			} else
			{
				m_infoMsg_thr("impossible hostsCount value = %d\n", hostsCount);
				errStatus = ERR_IMP;
				completion(&cmlArgs, errP);
			}
		}
	}
	if(err >= MAX_FAILS)
	{
		m_infoMsg_thr("sendto() or recvfrom() finaly faild\n");
		errStatus = -errno;
	}
	errStatus = ERR_FF;
	completion(&cmlArgs, errP);

	m_dbg1("end icmp\n"); //dbg
	return 0;
}


///---FUNC------FUNC------FUNC------FUNC------FUNC---

static unsigned short checksumICMP(void *b, int len)
{
	u_short *buf = (u_short*) b;
	unsigned int sum=0;
	unsigned short result;

	for ( sum = 0; len > 1; len -= 2 )
		sum += *buf++;
	if ( len == 1 )
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;

	return result;
}

static int completion(completionArgs_t *args, void *errP)
{
	m_infoMsg_thr("%s: exiting\n", args->ISPname);
	if(*(args->socksd) >=0 )
		if(close(*(args->socksd)))
			m_perrMsg_thr("close(socksd)");
	if(*(args->sockrd) >=0 )
		if(close(*(args->sockrd)))
			m_perrMsg_thr("close(socksd)");

	if(0 > chgDefRoutePriority(args->ipv4gate, *(args->idx), *(args->priority), RTM_DELROUTE))
		m_infoMsg_thr("Error: delete Route with priority\n");

	if(*(args->tidnextISP))
	{
		*(args->NextISPexitF) = 1; //условие завершения созданног потока
		if(0 != pthread_join(*(args->tidnextISP), NULL) )
			m_perrMsg_thr("Error pthread_join");
	}

	m_infoMsg_thr("%s: exit\n", args->ISPname);
	pthread_exit(errP);
}
